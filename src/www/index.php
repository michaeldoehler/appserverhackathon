<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$map = array(
    'AppserverTest\HttpModuleMyCustomServletEngine' => __DIR__ . '/../classes/HttpModuleMyCustomServletEngine.php',
    'AppserverTest\ServletAbstract' => __DIR__ . '/../classes/ServletAbstract.php',
    'AppserverTest\ServletIndex' => __DIR__ . '/../classes/ServletIndex.php',
    'AppserverTest\Session' => __DIR__ . '/../classes/Session.php',
    'AppserverTest\SessionHandler' => __DIR__ . '/../classes/SessionHandler.php',
    'AppserverTest\SessionManagerInterface' => __DIR__ . '/../classes/SessionManagerInterface.php',
    'AppserverTest\SessionManagerRedis' => __DIR__ . '/../classes/SessionManagerRedis.php',
    'AppserverTest\SessionManagerFilesystem' => __DIR__ . '/../classes/SessionManagerFilesystem.php',
);

foreach($map as $class => $file) {
    require_once $file;
}

//$sessionHandler = new \AppserverTest\SessionHandler(new \AppserverTest\SessionManagerRedis(new \Predis\Client('tcp://23.97.214.66:6379')));
//$sessionHandler = new \AppserverTest\SessionHandler(new \AppserverTest\SessionManagerRedis(new Predis\Client('tcp://127.0.0.1:6379')));
$sessionHandler = new \AppserverTest\SessionHandler(new \AppserverTest\SessionManagerFilesystem('/tmp'));
sleep(1);
$session = $sessionHandler->start($_COOKIE['SESSID']);
setcookie('SESSID', $sessionHandler->getSessionId());

if (isset($_REQUEST['logout']) && $session->getValue('isloggedin')) {
    $session->setValue('isloggedin', false);
}
if (isset($_REQUEST['username'])) {
    if ($_REQUEST['username'] == "test") {
        $session->setValue('isloggedin', true);
    }
}

if ($session->getValue('isloggedin') == true) {
    echo '<a href="?logout=true">Logout!</a>';
} else {
    echo "<form method='post' action='index.php'>User:<input type='text' name='username' /><br />Pass:<input type='password' name='password' /><br /><input type='submit' value='login' /></form>";
}

$sessionHandler->close();