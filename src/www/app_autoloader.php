<?php
/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 24.04.16
 * Time: 12:37
 */

$map = array(
    'AppserverTest\HttpModuleMyCustomServletEngine' => __DIR__ . '/../classes/HttpModuleMyCustomServletEngine.php',
    'AppserverTest\ServletAbstract' => __DIR__ . '/../classes/ServletAbstract.php',
    'AppserverTest\ServletIndex' => __DIR__ . '/../classes/ServletIndex.php',
    'AppserverTest\Session' => __DIR__ . '/../classes/Session.php',
    'AppserverTest\SessionHandler' => __DIR__ . '/../classes/SessionHandler.php',
    'AppserverTest\SessionManagerInterface' => __DIR__ . '/../classes/SessionManagerInterface.php',
    'AppserverTest\SessionManagerRedis' => __DIR__ . '/../classes/SessionManagerRedis.php',
    'AppserverTest\SessionManagerFilesystem' => __DIR__ . '/../classes/SessionManagerFilesystem.php',
);

spl_autoload_register(function ($class) use ($map) {

    if(isset($map[$class])){
        require $map[$class];
        return true;
    }

    return false;
});

require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';