<?php
/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 23.04.16
 * Time: 18:24
 */

require_once __DIR__ . '/../vendor/autoload.php';

$client = new Predis\Client('tcp://23.97.214.66:6379');
$client->set('foo', 'bar');
$value = $client->get('foo');

var_dump($value);