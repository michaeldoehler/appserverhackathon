<?php
namespace AppserverTest;

/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 23.04.16
 * Time: 22:37
 */
interface SessionManagerInterface
{

    public function hasSession($sessionId);

    public function getSession($sessionId);

    public function saveSession(Session $session);

    public function deleteSession(Session $session);

}