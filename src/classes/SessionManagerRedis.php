<?php
namespace AppserverTest;

/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 23.04.16
 * Time: 22:58
 */
class SessionManagerRedis implements SessionManagerInterface
{

    /**
     * @var \Predis\Client
     */
    private $redis;

    /**
     * SessionManagerRedis constructor.
     * @param \Predis\Client $redis
     */
    public function __construct(\Predis\Client $redis)
    {
        $this->redis = $redis;
    }

    public function hasSession($sessionId)
    {
        return is_array(json_decode($this->redis->get($sessionId), true));
    }

    public function getSession($sessionId)
    {
        $session = new Session($sessionId);
        $session->setValues(json_decode($this->redis->get($sessionId), true));
        return $session;
    }

    public function saveSession(Session $session)
    {
        $this->redis->set($session->getId(), json_encode($session->getValues()));
    }

    public function deleteSession(Session $session)
    {
        $this->redis->del(array($session->getId()));
    }
}