<?php
/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 24.04.16
 * Time: 12:51
 */

namespace AppserverTest;


abstract class ServletAbstract
{

    /**
     * @var \AppserverIo\Psr\HttpMessage\RequestInterface
     */
    private $request;

    /**
     * @var \AppserverIo\Psr\HttpMessage\ResponseInterface
     */
    private $response;

    /**
     * @var Session
     */
    private $session;

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param Session $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }

    /**
     * @return \AppserverIo\Psr\HttpMessage\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param \AppserverIo\Psr\HttpMessage\RequestInterface $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return \AppserverIo\Psr\HttpMessage\ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param \AppserverIo\Psr\HttpMessage\ResponseInterface $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function handleRequest()
    {
        $this->handle();
    }

    abstract public function handle();

    abstract public function getName();

}