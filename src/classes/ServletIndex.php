<?php
/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 24.04.16
 * Time: 12:53
 */

namespace AppserverTest;


class ServletIndex extends ServletAbstract
{

    public function handle()
    {

        if ($this->getRequest()->hasParam('logout') && $this->getSession()->getValue('isloggedin')) {
            $this->getSession()->setValue('isloggedin', false);
        }
        if ($this->getRequest()->hasParam('username')) {
            if ($this->getRequest()->getParam('username') == "test") {
                $this->getSession()->setValue('isloggedin', true);
            }
        }

        if ($this->getSession()->getValue('isloggedin') == true) {
            $this->getResponse()->appendBodyStream('<a href="/servletindex?logout=true">Logout!</a>');
        } else {
            $this->getResponse()->appendBodyStream("<form method='post' action='/servletindex'>User:<input type='text' name='username' /><br />Pass:<input type='password' name='password' /><br /><input type='submit' value='login' /></form>");
        }
    }

    public function getName()
    {
        return 'servletindex';
    }
}