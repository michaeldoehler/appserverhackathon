<?php

namespace AppserverTest;

use AppserverIo\Http\HttpCookie;
use AppserverIo\Http\HttpResponseStates;
use AppserverIo\Psr\HttpMessage\Protocol;
use AppserverIo\Psr\HttpMessage\RequestInterface;
use AppserverIo\Psr\HttpMessage\ResponseInterface;
use AppserverIo\Server\Dictionaries\ModuleHooks;
use AppserverIo\Server\Exceptions\ModuleException;
use AppserverIo\Server\Interfaces\RequestContextInterface;
use AppserverIo\Server\Interfaces\ServerContextInterface;
use AppserverIo\WebServer\Interfaces\HttpModuleInterface;

/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 24.04.16
 * Time: 10:07
 */
class HttpModuleMyCustomServletEngine implements HttpModuleInterface
{

    /**
     * Defines the module name.
     *
     * @var string MODULE_NAME
     */
    const MODULE_NAME = 'mycustomservletengine';

    /**
     * @var \AppserverTest\SessionHandler
     */
    public static $sessionHandler;

    public $sessionId;

    protected static $handlers = array();

    public function getServletByName($name)
    {
        $url = parse_url($name);
        if ($url['path'] == "/servletindex") {
            return new ServletIndex();
        }
        return null;
    }

    /**
     * Implements module logic for given hook
     *
     * @param \AppserverIo\Psr\HttpMessage\RequestInterface $request A request object
     * @param \AppserverIo\Psr\HttpMessage\ResponseInterface $response A response object
     * @param \AppserverIo\Server\Interfaces\RequestContextInterface $requestContext A requests context instance
     * @param int $hook The current hook to process logic for
     *
     * @return bool
     * @throws \AppserverIo\Server\Exceptions\ModuleException
     */
    public function process(RequestInterface $request, ResponseInterface $response, RequestContextInterface $requestContext, $hook)
    {

        // check if shutdown hook is comming
        if (ModuleHooks::SHUTDOWN === $hook) {
            return $this->shutdown($request, $response);
        }

        // if wrong hook is comming do nothing
        if (ModuleHooks::REQUEST_POST !== $hook) {
            //    return;
        }

        //if ($hook == ModuleHooks::REQUEST_POST) {

        $servlet = $this->getServletByName($request->getUri());

        if ($servlet !== null && $hook == 3) {
            $sessionId = null;
            if ($request->hasCookie('TEST-SESSIONID')) {
                $sessionId = $request->getCookie('TEST-SESSIONID')->getValue();
            }

            $session = self::$sessionHandler->start($sessionId);

            if (!$request->hasCookie('TEST-SESSIONID')) {
                $response->addCookie(new HttpCookie('TEST-SESSIONID', self::$sessionHandler->getSessionId()));
            }

            $response->addHeader(Protocol::HEADER_CONTENT_TYPE, 'text/html');

            $response->resetBodyStream();
            $response->setStatusReasonPhrase(null);
            $response->setStatusCode(200);
            $servlet->setRequest($request);
            $servlet->setResponse($response);
            $servlet->setSession($session);

            $servlet->handleRequest();

            self::$sessionHandler->close();

            return;
        }

        if ($servlet !== null && $hook == 4) {
            //self::$sessionHandler->close();
        }

    }

    /**
     * Return an array of module names which should be executed first
     *
     * @return array The array of module names
     */
    public function getDependencies()
    {
        return array();
    }

    /**
     * Returns the module name
     *
     * @return string The module name
     */
    public function getModuleName()
    {
        return self::MODULE_NAME;
    }

    /**
     * Initiates the module
     *
     * @param \AppserverIo\Server\Interfaces\ServerContextInterface $serverContext The server's context instance
     *
     * @return bool
     * @throws \AppserverIo\Server\Exceptions\ModuleException
     */
    public function init(ServerContextInterface $serverContext)
    {
        return true;
    }

    /**
     * Prepares the module for upcoming request in specific context
     *
     * @return bool
     * @throws \AppserverIo\Server\Exceptions\ModuleException
     */
    public function prepare()
    {
        self::$sessionHandler = new \AppserverTest\SessionHandler(new \AppserverTest\SessionManagerRedis(new \Predis\Client('tcp://23.97.214.66:6379')));
        //self::$sessionHandler = new \AppserverTest\SessionHandler(new \AppserverTest\SessionManagerFilesystem('/tmp'));
        sleep(1);

        return true;

    }
}