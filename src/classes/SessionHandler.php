<?php

namespace AppserverTest;

/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 23.04.16
 * Time: 22:42
 */
class SessionHandler
{

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @var Session
     */
    private $session;

    /**
     * SessionHandler constructor.
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(SessionManagerInterface $sessionManager)
    {
        $this->sessionManager = $sessionManager;
    }

    public function close()
    {
        if ($this->session !== null) {
            $this->sessionManager->saveSession($this->session);
        }
    }

    public function destroy($session_id)
    {
        if (null !== ($session = $this->sessionManager->getSession($session_id))) {
            $this->sessionManager->deleteSession($session);
        }
    }

    public function start($session_id)
    {
        if ($this->sessionManager->hasSession($session_id)) {
            $this->session = $this->sessionManager->getSession($session_id);
        } else {
            $this->session = new \AppserverTest\Session($session_id === null ? \Ramsey\Uuid\Uuid::uuid1()->toString() : $session_id);
        }

        return $this->session;
    }

    public function getSessionId()
    {
        return $this->session->getId();
    }
}