<?php
namespace AppserverTest;

/**
 * Created by PhpStorm.
 * User: michaeldoehler
 * Date: 23.04.16
 * Time: 22:58
 */
class SessionManagerFilesystem implements SessionManagerInterface
{

    private $directory;

    /**
     * SessionManagerFilesystem constructor.
     * @param $directory
     */
    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    private function getSessionFile($sessionId)
    {
        return $this->directory . DIRECTORY_SEPARATOR . $sessionId . '.json';
    }

    public function hasSession($sessionId)
    {
        return file_exists($this->getSessionFile($sessionId));
    }

    public function getSession($sessionId)
    {
        $session = new Session($sessionId);
        $session->setValues(json_decode(file_get_contents($this->getSessionFile($sessionId)), true));
        return $session;
    }

    public function saveSession(Session $session)
    {
        file_put_contents($this->getSessionFile($session->getId()), json_encode($session->getValues()));
    }

    public function deleteSession(Session $session)
    {
        unlink($this->getSessionFile($session->getId()));
    }
}